package com.laoba.meishijia.exception;

import com.laoba.meishijia.processdata.JsonData;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class MyExceptionAdvice {

    /**
     * 专门用于处理登录异常
     * @param e
     * @return
     */
    @ExceptionHandler(LoginException.class)
    public JsonData loginExceptionHandler(LoginException e){
        return new JsonData(e.getCode(),e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public JsonData  exceptionHandler(Exception e) {
        //记录异常日志
        //异常日志对于系统非常重要

        return new JsonData(1002,e.getMessage());
    }
}
