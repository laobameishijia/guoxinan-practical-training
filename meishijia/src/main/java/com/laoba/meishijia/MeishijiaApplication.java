package com.laoba.meishijia;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.laoba.meishijia.mapper")
public class MeishijiaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MeishijiaApplication.class, args);
    }

}
