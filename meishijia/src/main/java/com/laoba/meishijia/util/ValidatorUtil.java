package com.laoba.meishijia.util;

import com.laoba.meishijia.processdata.JsonData;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.List;

public class ValidatorUtil {

    /**
     * 展示错误信息
     */
    public static void showMsg(BindingResult result) throws Exception {
        List<ObjectError> allErrors = result.getAllErrors();

        for (ObjectError error : allErrors) {
            throw  new Exception(error.getDefaultMessage());
        }
    }
}
