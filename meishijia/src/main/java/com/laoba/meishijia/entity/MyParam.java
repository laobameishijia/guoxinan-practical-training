package com.laoba.meishijia.entity;

import java.sql.Timestamp;

public class MyParam {

    //参数私有化
    private String keywords;
    private Integer status;
    private String startTime;
    private String endTime;

    //无参构造器
    public MyParam(){

    }

    //get set 方法
    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
