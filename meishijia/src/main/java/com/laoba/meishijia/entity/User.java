package com.laoba.meishijia.entity;

// 用户实体类
//标准的JavaBean的格式
//1.所有属性私有化
//2.显示写出无参构造器
//3.所有属性的get 和set方法

/**
 *
 */
public class User {
    private String name;
    private String phone;
    private String address;
    private Integer qq;

    public User() {

    }

    public User(String name, String phone, String address, Integer qq) {
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.qq = qq;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getQq() {
        return qq;
    }

    public void setQq(Integer qq) {
        this.qq = qq;
    }
}
