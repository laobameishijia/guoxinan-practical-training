package com.laoba.meishijia.mapper;

import com.laoba.meishijia.entity.Admin;
import com.laoba.meishijia.entity.MyParam;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 主要用来操作数据库，增删改查
 */
@Mapper
public interface AdminMapper {

    /**
     * 查询所有数据
     */
    List<Admin> findAll();

    List<Admin> fineByParam(@Param("param") MyParam param);

    /**
     *增加
     * @param admin
     */
    void save(@Param("admin") Admin admin);

    /**
     * 真删除
     */
    void deleteTrue(@Param("delete") Integer id);

    /**
     * 更新
     * @param admin
     */
    void update(@Param("admin") Admin admin);

    /**
     * 软删除
     * @param id
     */
    void deleteFalse(@Param("id") Integer id);

    /**
     * 查询单条数据
     * @param id
     */
    Admin findone(@Param("id") Integer id);

    /**
     * 通过用户名查询对应的用户
     * @param name
     * @return
     */
    Admin findoneByName(@Param("name") String name);

    /**
     * 批量插入
     */
    int insertBatch(@Param("adminList") List<Admin> adminList);

    /**
     * 批量删除
     * @param adminList
     * @return
     */
    int deleteBatch(@Param("adminList") List<Admin> adminList);

}
