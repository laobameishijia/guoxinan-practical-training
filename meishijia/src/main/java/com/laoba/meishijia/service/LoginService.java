package com.laoba.meishijia.service;

import com.laoba.meishijia.entity.Admin;
import com.laoba.meishijia.processdata.JsonData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * 登录服务接口
 * 写接口的目的，就是为了不同类实现接口
 * 比如：可以有手机登录、验证码登录、账号密码登录
 */
public interface LoginService {

    /**
     * 用户登录
     * @param admin
     * @return
     */
    JsonData login (Admin admin, HttpSession session);
    JsonData exit (HttpServletRequest request);
}
