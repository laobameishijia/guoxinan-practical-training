package com.laoba.meishijia.service;


import com.laoba.meishijia.entity.Admin;
import com.laoba.meishijia.mapper.AdminMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private AdminMapper mapper;

    public Admin selectUserById(int id){
        return mapper.findone(id);
    }

    public boolean login(Admin admin){
        String name = admin.getAdminName();
        String password = admin.getAdminPwd();
        Admin u1 =  mapper.findoneByName(name);
        if(u1==null){
            return false;
        }else{
            if(u1.getAdminPwd().equals(password)){
                return true;
            }else{
                return false;
            }
        }
    }

}
