package com.laoba.meishijia.config;


import com.laoba.meishijia.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SystemConfig implements WebMvcConfigurer {
    /**
     * 专门用来注册拦截器的
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor())
                //拦截所有的请求
                .addPathPatterns("/**")
                //放行方法
                .excludePathPatterns("/login/**")
                //放行css
                .excludePathPatterns("/static/**");
    }
}
