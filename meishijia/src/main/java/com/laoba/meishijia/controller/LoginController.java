package com.laoba.meishijia.controller;


import com.laoba.meishijia.entity.Admin;
import com.laoba.meishijia.entity.User;
import com.laoba.meishijia.processdata.JsonData;
import com.laoba.meishijia.service.LoginService;
import com.laoba.meishijia.service.UserService;
import com.laoba.meishijia.service.impl.LoginServiceImpl;
import com.laoba.meishijia.util.ValidatorUtil;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private LoginServiceImpl loginService;

    @RequestMapping("/page")
    public String login() {
        return "login";
    }

    @PostMapping("/verify")
    @ResponseBody//不需要进行页面跳转而是直接返回数据。
    //添加了@ResponseBody注解的方法，返回值会通过HTTP响应主体直接发送给浏览器。
    public JsonData verifyLogin(@Validated Admin admin, BindingResult result, HttpSession session) throws Exception {
        //数据校验
        //获取所有错误
        ValidatorUtil.showMsg(result);

        return loginService.login(admin, session);
    }

    @PostMapping("/exit")
    @ResponseBody//不需要进行页面跳转而是直接返回数据。
    //添加了@ResponseBody注解的方法，返回值会通过HTTP响应主体直接发送给浏览器。
    public JsonData exit(HttpServletRequest request) throws Exception {

        return loginService.exit(request);
    }












//    @GetMapping("/page")
//    public String login(){
//        return "login";
//    }
//
////    @PostMapping("/index")
////    public String login_ture(){
////        return "index";
////    }
//
//    @RequestMapping(value = "/verify", method = {RequestMethod.POST})
//    public String verify(HttpServletRequest req, HttpServletResponse resp){
//
//        String username = req.getParameter("username");
//        String password = req.getParameter("password");
//
//
//        Admin user = new Admin();
//        user.setAdminName(username);
//        user.setAdminPwd(password);
//
//        System.out.println(user.toString());
//        boolean flag = userService.login(user);
//
//        if(flag==true){
//            System.out.println("登录成功");
//            return "index";
//        }else {
//            System.out.println("登录失败");
//            return "login";
//        }
//
//    }
}
