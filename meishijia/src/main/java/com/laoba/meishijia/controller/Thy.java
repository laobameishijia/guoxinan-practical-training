package com.laoba.meishijia.controller;

import com.laoba.meishijia.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/thy")
public class Thy {

    /**
     * 渲染的主页
     * @return
     */
    @GetMapping("/index")
    public String index(){
        //这个名字要跟template下面的那个html文件的名字是一致的
        return "index";
    }

    /**
     * 渲染表格
     * @return
     */
    @GetMapping("/tab")
    public String table(Model model){
        //将数据发送给模板
        //数据存放在model里面
        model.addAttribute("title","湖人总冠军");

        //这个地方模拟数据库的数据查询
        //准备模拟数据
        List<User> users = new ArrayList<>();
        users.add(new User("詹姆斯1","123456","美国",1));
        users.add(new User("詹姆斯2","12345","美国",2));
        users.add(new User("詹姆斯3","1234","美国",3));
        users.add(new User("詹姆斯4","123","美国",4));
        users.add(new User("詹姆斯5","12","美国",5));

        model.addAttribute("users",users);

        return "table";
    }

}
