package com.laoba.meishijia.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import com.laoba.meishijia.entity.User;
import com.laoba.meishijia.processdata.JsonData;
import org.springframework.web.bind.annotation.*;

@Controller
public class IndexController {

    /**
     * 首页
     * @return
     */
//    @GetMapping({"/index","/"})
    @RequestMapping(value = "/index", method = {RequestMethod.GET,RequestMethod.POST})
    public String index(){
        return "index";
    }

    /**
     * 欢迎页
     * @return
     */
    @GetMapping("/welcome")
    public String  welcome(){
        return "welcome";
    }




    @GetMapping("/getname")
    public JsonData getname(@RequestParam(required = false) String name){
        return new JsonData(200,"success",name);
    }

    @GetMapping("/getphone")
    public JsonData getphtone(@RequestParam(required = false, name="test", defaultValue = "123456") String phone){
        return new JsonData(200,"success",phone);
    }


    //TODO 这个地方有问题，接受不了参数
    //TODO 看一下这个软件如何写那种注释
//    @GetMapping("/get/{id}")//这样写不行
    @GetMapping("/get")//这样写不行
    public JsonData getpar(@PathVariable @RequestParam(required = false) Integer id){
        return new JsonData(200, "success", id);
//        return "test";
    }

    //接受复杂参数
    @PostMapping("/getuser")
    public JsonData regUser(User user){
        return new JsonData(200,"success",user);
    }

    //接受json数据

    @PostMapping("/getuser2")
    public JsonData regUser2(@RequestBody User user){
        return new JsonData(200, "success",user);
    }
}
