package com.laoba.meishijia;

import com.laoba.meishijia.entity.Admin;
import com.laoba.meishijia.entity.MyParam;
import com.laoba.meishijia.mapper.AdminMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class MeishijiaApplicationTests {

    @Autowired
    private AdminMapper mapper;
    @Test
    void contextLoads() {
    }
    @Test
    public void testFindAll() {
        List<Admin> admins = mapper.findAll();

        for (Admin admin : admins) {
            System.out.println(admin);
        }

    }

    @Test
    public void testFindone(){
        Admin admin = mapper.findone(1);
        System.out.println(admin);
    }

    @Test
    public void testInsert() {
        Admin admin = new Admin();
        admin.setAdminName("james");
        admin.setAdminPwd("123");
        admin.setAdminPhone(123L);
        admin.setCreateTime(new Timestamp(System.currentTimeMillis()));
        mapper.save(admin);
    }

    @Test
    public void testDelete(){
        mapper.deleteFalse(1);
    }

    @Test
    public void testupdate() {
        Admin admin = new Admin();
        admin.setAdminName("james");
        admin.setAdminPwd("123");
        admin.setAdminPhone(123L);
        admin.setCreateTime(new Timestamp(System.currentTimeMillis()));
        mapper.update(admin);
    }

    @Test
    public void findParam(){
        MyParam myParam = new MyParam();

        myParam.setKeywords("jame");

        List<Admin> admins = mapper.fineByParam(myParam);

        for(Admin admin : admins){
            System.out.println(admin);
        }

    }

    /**
     * 批量插入数据
     */
    @Test
    public void insertBatch() {

        List<Admin> adminList = new ArrayList<>();
        Admin admin;
        for(int i = 0; i < 10; i++){
            admin = new Admin();
            admin.setAdminName("詹姆斯"+ i);
            admin.setCreateTime(new Timestamp(System.currentTimeMillis()));
            adminList.add(admin);
        }

        mapper.insertBatch(adminList);
    }

    /**
     * 批量删除
     */
    @Test
    public void deleteBatch() {
        List<Admin> adminList = new ArrayList<>();
        Admin admin;
        for(int i = 0; i < 10; i++){
            admin = new Admin();
            admin.setAdminName("詹姆斯"+ i);
            admin.setCreateTime(new Timestamp(System.currentTimeMillis()));
            adminList.add(admin);
        }
        mapper.deleteBatch(adminList);
    }
}
